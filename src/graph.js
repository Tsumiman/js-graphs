"use strict";

( function( module, document) {

    var DOUBLE_CHECK_COLLISIONS = true; // Check if vertices collide. Guarantees that there will be no overlaps. May be quite inefficient.

    var Graph = function(){
        this.vertices = {};
        this.edges    = {};
    }
    Graph.prototype = {
        constructor: Graph
        ,
        edgeColor: 'rgba(196, 128, 128, 1)'
        ,
        addVertex: function( vertex ){
            
            this.vertices[vertex.id] = vertex;
            
            return this;
        }
        ,
        addEdge: function( edge ) {

            this.edges[edge.id] = edge;
                        
            return this;
        }
        ,
        draw: function ( context ){
            return this;
        },
        checkCollisions: function( vertex ){
            var allIsFine     = false;
            var iterations    = 0;
            var maxIterations = 2000;
            while (!allIsFine && iterations < maxIterations){
                iterations++;
                allIsFine = true;
                for(var obj in this.vertices){
                    if (vertex.id != this.vertices[obj].id){
                        var dX = vertex.x-this.vertices[obj].x;
                        var dY = vertex.y-this.vertices[obj].y;
                        var dist    = module.dist( 0, 0, dX, dY );
                        var mindist = (vertex.radius + this.vertices[obj].radius); 

                        if (dist < mindist){
                            allIsFine = false;
                            vertex.move(dX*(mindist/dist)*1.1+this.vertices[obj].x, dY*(mindist/dist)*1.1+this.vertices[obj].y);
                        }
                    }
                }

                allIsFine = allIsFine || !DOUBLE_CHECK_COLLISIONS;
            }
        }
        ,
        hitPoint: function( x, y ){
            return false;
        }
    }

    module.Graph = Graph;

}( graph || {}, document ) );
