"use strict";

/**
 * Graph module
 * 
 * @param  {document} document
 * @return {Object} Graph module
 */
var graph = function(document) {

    var module = {
        dist: function( x1, y1, x2, y2 ) {
            var dX = Math.abs( x1 - x2 );
            var dY = Math.abs( y1 - y2 );

            return Math.sqrt( dX*dX + dY*dY );
        }
    };
    return module;
    
}(document);
