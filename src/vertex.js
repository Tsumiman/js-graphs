"use strict";

( function( module, document) {

    var VERTEX_RADIUS    = 15;
    var VERTEX_BAR_WIDTH = 5;
    
    /**
     * Graph Vertex class
     * 
     * @param {int} x
     * @param {int} y
     * @param {int} radius
     * @constructor
     */
    var Vertex = function(id) {
        this.x      = 0;
        this.y      = 0;
        this.radius = VERTEX_RADIUS; 
        this.id     = id;
        
        this.progress = Math.random();
    }

    Vertex.prototype = {
        constructor : Vertex
        ,
        /** {int} */
        taskId: null
        ,
        /** {string} */
        color: "rgba(128, 160, 196, 1)"
        ,
        progressColor: 'rgba(64, 196, 64, 0.5)'
        ,
        /**
         * 
         * @param   {int}   r
         * @param   {int}   g
         * @param   {int}   b
         * @param   {float} a
         * 
         * @return {Vertex}  self
         */
        setColor: function( r, g, b, a ) {
            this.color = 'rgba(' + r + ',' + g + ',' + b + ',' + (a || 1) + ')';
            
            return this;
        }
        ,
        /**
         * Draw node on given canvas context
         * 
         * @param {context} context
         * 
         * @return {Vertex}  self
         */
        draw: function( context ){
            var radius = this.radius - Math.floor( VERTEX_BAR_WIDTH/2 ) - 2;
            var prevLineWidth = context.lineWidth;
            //draw vertex
            context.beginPath();
            context.arc( this.x, this.y, radius, 0, 2 * Math.PI, false);
            context.fillStyle = this.color;
            context.closePath();
            context.fill();
            //draw progress bar
            context.beginPath();
            context.arc( this.x, this.y, this.radius, 0, this.progress * ( 2 * Math.PI), false);
            context.strokeStyle = this.progressColor;
            context.lineWidth   = VERTEX_BAR_WIDTH;
            
            context.stroke();
            context.lineWidth = prevLineWidth;
            return this;
        }
        ,
        /**
         * 
         * @param {int} x
         * @param {int} y
         * 
         * @return {Vertex}  self
         */
        move: function( x,y ){
            this.x = x;
            this.y = y;
            
            return this;
        },
        /**
         * Check for intersection with a point
         * 
         * @param  {int} x
         * @param  {int} y
         * @return {bool}
         */
        hitPoint: function( x, y ){           
            return this.radius >= module.dist(x, y, this.x, this.y); 
        }
    };

    module.Vertex = Vertex;

} (graph || {}, document) )
