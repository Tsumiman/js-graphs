"use strict";

( function( module, document) {
    
    var CHECK_COLLISIONS = true; // Check if vertices collide. O(V)

    /**
     * Board class. Handle drag`n`drop, select and other stuff
     * 
     * @param element
     * @constructor
     */
    var Board = function( element ) {
        this.element = element;
        this.objects = [];

        if( element.getContext ){
            this.context = element.getContext( '2d' );
        }
        
        this.init();
    };
    Board.prototype = {
        constructor: Board
        ,
        element: null
        ,
        elementPos: null
        ,
        bufferElement: null
        ,
        context: null
        ,
        bufferContext: null
        ,
        objects: null
        ,
        draggedObj: null
        ,
        selectedObj: null
        ,
        init: function() {
            //bind event handlers
            this.element.addEventListener( 'mousedown', this );
            this.element.addEventListener( 'mouseup', this );
            
            this.elementPos = this.element.getBoundingClientRect();
            
            this.bufferElement = document.createElement('canvas');
            this.bufferElement.width  = this.element.width;
            this.bufferElement.height = this.element.height;
            
            this.bufferContext = this.bufferElement.getContext( '2d' );
            
        }
        ,
        add: function( object ) {
            this.objects.push( object );
        }
        ,
        draw: function() {
            this.clearContext();
            
            for ( var i = 0; i < this.objects.length; i++ ){
                this.objects[i].draw( this.bufferContext );
            }
            
            this.context.drawImage( this.bufferElement, 0, 0 );
        }
        ,
        clearContext: function(){
            this.context.clearRect(0, 0, this.element.width, this.element.height);
            this.bufferContext.clearRect(0, 0, this.element.width, this.element.height);
        }
        ,
        handleEvent: function( event ) {
            switch( event.type ) {
                case 'mousedown':
                    this._mouseDown( event );
                    break;
                case 'mouseup':
                    this._mouseUp( event );
                    break;
                case 'mousemove':
                    this._mouseMove( event );
                    break;
            }
        }
        ,
        _mouseDown: function( event ) {
            if (event.button != 0) return;
            var x = event.pageX - this.elementPos.left;
            var y = event.pageY - this.elementPos.top;

            var i = this.getObjectByPos( x, y );
            if( i > -1 ){
                this.draggedObj = this.objects[i];
                this.element.addEventListener( 'mousemove', this );
            }
        }
        ,
        _mouseUp: function( event ) {
            this.draggedObj = null;
            this.element.removeEventListener( 'mousemove', this );
        }
        ,
        _mouseMove: function( event ) {            
            if( this.draggedObj ){
                var x = event.pageX - this.elementPos.left;
                var y = event.pageY - this.elementPos.top;

                this.draggedObj.move( x, y );
                if (CHECK_COLLISIONS) this.objects[0].checkCollisions(this.draggedObj);
                this.draw();
            }
        }
        ,
        getObjectByPos: function( x, y ) {
            var selected = -1;
            for ( var i = 0; i < this.objects.length; i++ ){
                if( this.objects[i].hitPoint( x, y ) == true ){
                    selected = i;
                    break;
                }
            }
            
            return selected;
        }

    };

    module.Board = Board;

}( graph || {}, document ) );
