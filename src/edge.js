"use strict";

( function( module, document) {

    var EDGE_WIDTH     = 3;
    var EDGE_ARROW_LEN = 10;
    var EDGE_SELECT_DIST = 7;
    var DEFAULT_COLOR = "rgba(128, 160, 196, 1)";
    
    /**
     * Graph Edge class
     * 
     * @param {int} x
     * @param {int} y
     * @param {int} radius
     * @constructor
     */
    var Edge = function(fV, tV, id) {
        this.fromVertex   = fV;
        this.toVertex     = tV;
        this.px = -1;
        this.py = -1;
        this.width = EDGE_WIDTH; 
        this.id     = id;

    }

    Edge.prototype = {
        constructor : Edge
        ,
        /** {int} */
        taskId: null
        ,
        /** {string} */
        color: "rgba(128, 160, 196, 1)"
        ,
        /**
         * 
         * @param   {int}   r
         * @param   {int}   g
         * @param   {int}   b
         * @param   {float} a
         * 
         * @return {Edge}  self
         */
        setColor: function( r, g, b, a ) {
            this.color = 'rgba(' + r + ',' + g + ',' + b + ',' + (a || 1) + ')';
            
            return this;
        }
        ,
        /**
         * Draw edge on given canvas context
         * 
         * @param {context} context
         * 
         * @return {Edge}  self
         */
        draw: function( context ){
            var angle = Math.atan2( this.fromVertex.y - this.toVertex.y, this.fromVertex.x - this.toVertex.x );
            var sX = this.fromVertex.x - this.fromVertex.radius * Math.cos( angle ),
                sY = this.fromVertex.y - this.fromVertex.radius * Math.sin( angle ),
                eX = this.toVertex.x + this.toVertex.radius * Math.cos( angle ),
                eY = this.toVertex.y + this.toVertex.radius * Math.sin( angle );
            
            context.beginPath();
            context.lineWidth=this.width;
            context.strokeStyle = this.color;

            //drawing line
            context.moveTo( sX, sY );
            if (this.px == -1 || this.py == -1) context.lineTo( eX, eY );
            else {
                angle = Math.atan2( this.fromVertex.y - this.py, this.fromVertex.x - this.px );
                sX = this.fromVertex.x - this.fromVertex.radius * Math.cos( angle ),
                sY = this.fromVertex.y - this.fromVertex.radius * Math.sin( angle ),
                angle = Math.atan2( this.py - this.toVertex.y, this.px - this.toVertex.x );
                eX = this.toVertex.x + this.toVertex.radius * Math.cos( angle ),
                eY = this.toVertex.y + this.toVertex.radius * Math.sin( angle );
                context.moveTo( sX, sY );
                context.quadraticCurveTo(this.px, this.py, eX, eY)
            }
            //drawing arrow
            var aX    = eX + EDGE_ARROW_LEN * Math.cos( angle + Math.PI/6 );
            var aY    = eY + EDGE_ARROW_LEN * Math.sin( angle + Math.PI/6 );
            context.lineTo( aX, aY);

            aX = eX + EDGE_ARROW_LEN * Math.cos( angle - Math.PI/6 );
            aY = eY + EDGE_ARROW_LEN * Math.sin( angle - Math.PI/6 );
            context.moveTo( eX, eY );
            context.lineTo( aX, aY);
            
            context.stroke();
            context.closePath();

            return this;
        }
        ,
        /**
         * 
         * @param {int} x
         * @param {int} y
         * 
         * @return {Edge}  self
         */
        move: function( x, y ){
            this.px = x; this.py = y;
        },
        /**
         * Check for intersection with a point
         * 
         * @param  {int} x
         * @param  {int} y
         * @return {bool}
         */
        hitPoint: function( x, y ){
            var angle = Math.atan2( this.fromVertex.y - this.toVertex.y, this.fromVertex.x - this.toVertex.x );
            var sX = this.fromVertex.x - this.fromVertex.radius * Math.cos( angle ),
                sY = this.fromVertex.y - this.fromVertex.radius * Math.sin( angle ),
                eX = this.toVertex.x + this.toVertex.radius * Math.cos( angle ),
                eY = this.toVertex.y + this.toVertex.radius * Math.sin( angle );

            var d = module.dist(sX, sY, eX, eY);

            var sina = (eY-sY)/d,
                cosa = (eX-sX)/d;

            var cX = (x-sX)*cosa+(y-sY)*sina,
                cY = -(x-sX)*sina+(y-sY)*cosa;
            if ( cX < 0 || cX > d ) return false;
            return Math.abs(cY) < EDGE_SELECT_DIST;
        }
    };

    module.Edge = Edge;

} (graph || {}, document) )
